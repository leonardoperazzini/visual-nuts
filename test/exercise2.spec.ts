import test from 'japa'
import supertest from 'supertest'

const BASE_URL = `http://${process.env.HOST}:${process.env.PORT}`

test.group('Exercise2', () => {
  test('ensure route works', async (assert) => {
    const response = await supertest(BASE_URL).get('/exercise2').expect(200)

    assert.deepEqual(response.body, {
      question1: 5,
      question2: {
        country: 'BE',
        languages: ['nl', 'fr', 'de'],
      },
      question3: 6,
      question4: {
        country: 'BE',
        languages: ['nl', 'fr', 'de'],
      },
      question5: 'de',
    })
  })
})
