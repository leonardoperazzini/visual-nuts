import test from 'japa'
import supertest from 'supertest'

const BASE_URL = `http://${process.env.HOST}:${process.env.PORT}`

test.group('Exercise1', () => {
  test('ensure route works', async (assert) => {
    const response = await supertest(BASE_URL).get('/exercise1').expect(200)

    let amountMod3 = 0
    let amountMod5 = 0
    let amountMod15 = 0

    for (let index = 1; index < 100 + 1; index++) {
      if (index % 3 === 0 && index % 15 !== 0) {
        amountMod3++
      }
      if (index % 5 === 0 && index % 15 !== 0) {
        amountMod5++
      }
      if (index % 15 === 0) {
        amountMod15++
      }
    }

    response.body.forEach((el) => {
      if (el === 'Visual') {
        amountMod3--
      }
      if (el === 'Nuts') {
        amountMod5--
      }
      if (el === 'Visual Nuts') {
        amountMod15--
      }
    })

    assert.equal(amountMod3, 0)
    assert.equal(amountMod5, 0)
    assert.equal(amountMod15, 0)
  })
})
