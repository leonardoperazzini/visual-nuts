declare module '@ioc:App/Services/Exercise2' {
  import { Country } from 'App/Types/Exercise2/Country'

  export default interface Exercise2 {
    setContries(contries: Array<Country>): void
    amountCountries(): number
    findCountryWithMostLangsSpeakingSpecificLang(lang: string): Country | null
    countAllLangs(): number
    findCountryWithHighestLangs(): Country | null
    findMostCommonLang(): string
  }
}
