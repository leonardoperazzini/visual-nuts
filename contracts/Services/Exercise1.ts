declare module '@ioc:App/Services/Exercise1' {
  export default interface Exercise1 {
    print(number: number): number[]
  }
}
