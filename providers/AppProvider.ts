import { ApplicationContract } from '@ioc:Adonis/Core/Application'
import Exercise1Service from 'App/Services/Exercise1'
import Exercise2Service from 'App/Services/Exercise2'

export default class AppProvider {
  constructor(protected app: ApplicationContract) {}

  public register() {
    this.app.container.bind('@ioc:App/Services/Exercise1', () => {
      return new Exercise1Service()
    })
    this.app.container.bind('@ioc:App/Services/Exercise2', () => {
      return new Exercise2Service()
    })
  }

  public async boot() {
    // IoC container is ready
  }

  public async ready() {
    // App is ready
  }

  public async shutdown() {
    // Cleanup, since app is going down
  }
}
