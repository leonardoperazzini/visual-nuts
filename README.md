# About the application

The application is made in Adonis.js V5.

# Development environment

To run the project you need Node (14.17.6^) and Npm(1.22.4^) installed. Run the following commands

```
yarn
node ace serve --watch
```

# Production environment

The application was deployed on the Heroku platform. The host is https://visual-nuts-challenge.herokuapp.com/

## Routes
 
GET - /exercise1
GET - /exercise2