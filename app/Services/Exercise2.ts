import Exercise2nterface from '@ioc:App/Services/Exercise2'
import { Country } from 'App/Types/Exercise2/Country'

export default class Exercise2 implements Exercise2nterface {
  protected contries: Array<Country>

  public setContries(contries: Array<Country>): void {
    this.contries = contries
  }

  public amountCountries(): number {
    return this.contries.length
  }

  public findCountryWithMostLangsSpeakingSpecificLang(lang: string): Country | null {
    let maxLangs = 0
    let countryWithMostLangs: Country | null = null

    this.contries.map((country) => {
      let found = country.languages.find((language) => {
        return lang === language
      })

      if (found === lang && country.languages.length > maxLangs) {
        maxLangs = country.languages.length
        countryWithMostLangs = country
      }
    })

    if (countryWithMostLangs) {
      return countryWithMostLangs
    } else {
      return null
    }
  }

  public countAllLangs(): number {
    const langs: Array<string> = []

    this.contries.map((country) => {
      langs.push(...country.languages)
    })

    const uniqueLangs = [...new Set(langs)]

    return uniqueLangs.length
  }

  public findCountryWithHighestLangs(): Country | null {
    let maxLangs = 0
    let countryWithMostLangs: Country | null = null

    this.contries.map((country) => {
      if (country.languages.length > maxLangs) {
        maxLangs = country.languages.length
        countryWithMostLangs = country
      }
    })

    if (countryWithMostLangs) {
      return countryWithMostLangs
    } else {
      return null
    }
  }

  public findMostCommonLang(): string {
    const langs: Array<string> = []

    this.contries.map((country) => {
      langs.push(...country.languages)
    })

    return this.getMostFrequent(langs)
  }

  private getMostFrequent(arr) {
    const hashmap = arr.reduce((acc, val) => {
      acc[val] = (acc[val] || 0) + 1
      return acc
    }, {})
    return Object.keys(hashmap).reduce((a, b) => (hashmap[a] > hashmap[b] ? a : b))
  }
}
