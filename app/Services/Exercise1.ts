import Exercise1Interface from '@ioc:App/Services/Exercise1'

export default class Exercise1 implements Exercise1Interface {
  protected items: Array<any>
  protected modFunctions: Array<string> = ['printVisual', 'printNuts', 'printVisualNuts']

  public print(number: number): Array<any> {
    this.initItems()
    this.iteratesUpToNumber(number)
    return this.items
  }

  private initItems(): void {
    this.items = []
  }

  private iteratesUpToNumber(number: number): void {
    for (let index = 1; index < number + 1; index++) {
      let item: any = index
      let itemAsString: string = this.decideWhichReturnItem(index)
      if (itemAsString !== '') {
        item = itemAsString
      }
      this.items.push(item)
    }
  }

  private decideWhichReturnItem(index: number): string {
    let item: string = ''

    this.modFunctions.forEach((func: string): void => {
      item = this[func](index, item)
    })

    return item
  }

  private printVisual(index: number, item: string): string {
    if (index % 3 === 0) {
      return 'Visual'
    }
    return item
  }

  private printNuts(index: number, item: string): string {
    if (index % 5 === 0) {
      return 'Nuts'
    }
    return item
  }

  private printVisualNuts(index: number, item: string): string {
    if (index % 15 === 0) {
      return 'Visual Nuts'
    }
    return item
  }
}
