import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { inject } from '@adonisjs/fold'
import Exercise1ServiceInterface from '@ioc:App/Services/Exercise1'

@inject(['@ioc:App/Services/Exercise1'])
export default class Exercise1Controller {
  private exercise1Service: Exercise1ServiceInterface

  constructor(exercise1Service: Exercise1ServiceInterface) {
    this.exercise1Service = exercise1Service
  }
  public async index({ response }: HttpContextContract) {
    const list = this.exercise1Service.print(100)
    response.send(list)
  }
}
