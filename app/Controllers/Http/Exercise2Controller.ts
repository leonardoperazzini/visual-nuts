import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { inject } from '@adonisjs/fold'
import Exercise2ServiceInterface from '@ioc:App/Services/Exercise2'
import { Country } from 'App/Types/Exercise2/Country'

@inject(['@ioc:App/Services/Exercise2'])
export default class Exercise2Controller {
  private exercise2Service: Exercise2ServiceInterface

  constructor(exercise2Service: Exercise2ServiceInterface) {
    this.exercise2Service = exercise2Service
  }
  public async index({ response }: HttpContextContract) {
    this.exercise2Service.setContries([
      {
        country: 'US',
        languages: ['en'],
      },
      {
        country: 'BE',
        languages: ['nl', 'fr', 'de'],
      },
      {
        country: 'NL',
        languages: ['nl', 'fy'],
      },
      {
        country: 'DE',
        languages: ['de'],
      },
      {
        country: 'ES',
        languages: ['es'],
      },
    ])

    const amount: number = this.exercise2Service.amountCountries()

    const countryWithMostLangsSpeakingGermany: Country | null =
      this.exercise2Service.findCountryWithMostLangsSpeakingSpecificLang('de')

    const countAllLangs: number = this.exercise2Service.countAllLangs()

    const countryWithHighestLangs: Country | null =
      this.exercise2Service.findCountryWithHighestLangs()

    const mostCommonLang: string = this.exercise2Service.findMostCommonLang()

    response.send({
      question1: amount,
      question2: countryWithMostLangsSpeakingGermany,
      question3: countAllLangs,
      question4: countryWithHighestLangs,
      question5: mostCommonLang,
    })
  }
}
