export type Country = {
  country: string
  languages: Array<string>
}
